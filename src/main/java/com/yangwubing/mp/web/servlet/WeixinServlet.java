package com.yangwubing.mp.web.servlet;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sd4324530.fastweixin.handle.EventHandle;
import com.github.sd4324530.fastweixin.handle.MessageHandle;
import com.github.sd4324530.fastweixin.message.BaseMsg;
import com.github.sd4324530.fastweixin.message.TextMsg;
import com.github.sd4324530.fastweixin.message.req.BaseEvent;
import com.github.sd4324530.fastweixin.message.req.BaseReqMsg;
import com.github.sd4324530.fastweixin.message.req.TextReqMsg;
import com.github.sd4324530.fastweixin.servlet.WeixinServletSupport;

public class WeixinServlet extends WeixinServletSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory
			.getLogger(WeixinServlet.class);
	private static final String TOKEN = "asdf12345";

	// 设置TOKEN，用于绑定微信服务器
	@Override
	protected String getToken() {
		return TOKEN;
	}

	// 使用安全模式时设置：APPID
	@Override
	protected String getAppId() {
		return "wx03eb21d7ae509e94";
	}

	// 使用安全模式时设置：密钥
	@Override
	protected String getAESKey() {
		return "Lef10OLwQggXDSirL9gL1zK2iH1dXhCNF1hD9NsGkjf";
	}

	// 重写父类方法，处理对应的微信消息
	@Override
	protected BaseMsg handleTextMsg(TextReqMsg msg) {
		String content = msg.getContent();
		log.debug("用户发送到服务器的内容:{}", content);
		return new TextMsg("服务器回复用户消息!");
	}

	// 1.1版本新增，重写父类方法，加入自定义微信消息处理器
	@Override
	protected List<MessageHandle> getMessageHandles() {
		List<MessageHandle> handles = new ArrayList<MessageHandle>();
		handles.add(new MessageHandle() {

			public BaseMsg handle(BaseReqMsg message) {
				// TODO Auto-generated method stub
				return null;
			}

			public boolean beforeHandle(BaseReqMsg message) {
				// TODO Auto-generated method stub
				return false;
			}

		});
		return handles;
	}

	// 1.1版本新增，重写父类方法，加入自定义微信事件处理器
	@Override
	protected List<EventHandle> getEventHandles() {
		List<EventHandle> handles = new ArrayList<EventHandle>();
		handles.add(new EventHandle() {

			public BaseMsg handle(BaseEvent event) {
				// TODO Auto-generated method stub
				return null;
			}

			public boolean beforeHandle(BaseEvent event) {
				// TODO Auto-generated method stub
				return false;
			}

		});
		return handles;
	}
}
